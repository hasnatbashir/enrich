from transformation import Transform

t = Transform('./data/WLLsentimentAnalysis3M.csv')

# Remove columns
t.data = t.data.drop(['Phone #.1', 'Unnamed: 8', 'Unnamed: 9', 'Unnamed: 10', 'Unnamed: 11'], axis=1)

# Remove Multiline
t.remove_multiline(t.data.columns, False, False, False)

# Remove Unicode Characters
t.remove_unicode_characters(t.data.columns, False, False, False)

# Remove links from campaign column
t.data = t.remove_rows_substring(['Campaign'], 'http:', False, False, False)

# Add state phone no. column
t.lookup_phone_state(['Phone #'], True, False, False)

# Save transformed data
print(t.data[['Phone #', 'Phone #_STATE']])
t.data.to_csv('./data/WLLsentimentAnalysis3M_transformed.csv', index=False)
