import numpy as  np
import pandas as pd
import os
import ast
import pymssql, sqlalchemy
def jaccard(string1, string2):
    intersection = len(list(set(string1).intersection(string2)))
    union = (len(string1) + len(string2)) - intersection
    return float(intersection) / union

def cosine_similarity(string1, string2):
    all_list = list(set(string1) | set(string2))
    arr_1 = np.array([1 if a in string1 else 0 for a in all_list])
    arr_2 = np.array([1 if a in string2 else 0 for a in all_list])
    dot_prod = np.dot(arr_1, arr_2)
    mag_prod = np.linalg.norm(arr_1) * np.linalg.norm(arr_2)
    return dot_prod/mag_prod

def make_groups(values):
    groups = []
    for i in values:
        if any(i in x for x in groups):
            continue
        group = [i]
        for j in values:
            if i == j:
                continue
            if (not any(j in x for x in groups)) and cosine_similarity(i, j) * 100 >= 85.0:
                group.append(j)
        groups.append(group)
    return groups


def save_groups(values, filepath= './data/recommendations.csv'):
    groups_dict = {}
    new_groups = make_groups(values)
    for g in new_groups:
        for gr in g:
            groups_dict[gr] = g[0]
    if not os.path.isfile(filepath):
        groups_df = pd.DataFrame({"source":new_groups, "target":pd.Series(list(groups_dict.values())).unique()})
        groups_df.to_csv(filepath, index=False)
        return groups_df
    file_groups = pd.read_csv(filepath)
    file_groups_dict = file_groups.set_index('target').to_dict()['source']
    target = file_groups_dict.keys()
    for new_group in new_groups:
        added = False
        for tar in target:
            if isinstance(file_groups_dict[tar], str):
                lst = ast.literal_eval(file_groups_dict[tar]) # Group list
            else:
                lst = file_groups_dict[tar]
            if cosine_similarity(new_group[0], tar) * 100 >= 85.0: # If value is similar to target value of group
                # lst = ast.literal_eval(file_groups_dict[tar]) # Group list
                lst.extend(new_group) # Add new group value(s) to the group
                file_groups_dict[tar] = list(set(lst)) # Add the new group list to the ditionary of groups
                added = True
                break # Break the loop since we have found a target group to add new values
            # Check similarity with values in group.
            for l in lst:
                if cosine_similarity(new_group[0], l) * 100 >= 85.0:
                    # lst = ast.literal_eval(file_groups_dict[tar]) # Group list
                    lst.extend(new_group) # Add new group value(s) to the group
                    file_groups_dict[tar] = list(set(lst)) # Add the new group list to the ditionary of groups
                    added = True
                    break # Break the loop since we have found a target group to add new values
        if not added: # If the group is not added in any existing targets. add it as a new group
            file_groups_dict[new_group[0]] = new_group
        

    groups_df = pd.DataFrame({"source":list(file_groups_dict.values()), "target":list(file_groups_dict.keys())})
    groups_df.to_csv(filepath, index=False)
    return groups_df


def recommendation_dict(values, filepath= './data/recommendations.csv'):

    groups_df = save_groups(values, filepath)

    return groups_df.set_index('target').to_dict()['source']


def get_db_df(source_col, target_col):
    return pd.DataFrame({'SOURCE_VALUE':source_col, 'TARGET_VALUE':target_col
        ,'APPEARANCE_LOGO' : None
        ,'APPEARANCE_COLOR' : None
        ,'USER_NAME_INSERT' : None
        ,'USER_NAME_UPDATE' : None
        ,'SERVER_INSERT_DATE' : None
        ,'SERVER_INSERT_TIME' : None
        ,'SERVER_INSERT_TIME_ZONE' : None
        ,'SERVER_UPDATE_DATE' : None
        ,'SERVER_UPDATE_TIME' : None
        ,'SERVER_UPDATE_TIME_ZONE' : None
        ,'CLIENT_INSERT_DATE' : None
        ,'CLIENT_INSERT_TIME' : None
        ,'CLIENT_INSERT_TIME_ZONE' : None
        ,'CLIENT_UPDATE_DATE' : None
        ,'CLIENT_UPDATE_TIME' : None
        ,'CLIENT_UPDATE_TIME_ZONE' : None
        ,'BSTATUS' : None
        ,'BDELETE' : None
        ,'BMAP' : None
        ,'REMARKS_1' : None
        ,'REMARKS_2' : None
        ,'REMARKS_3' : None
        ,'REMARKS_4' : None
        ,'FLEX_1' : None
        ,'FLEX_2' : None
        ,'FLEX_3' : None
        ,'FLEX_4' : None
        ,'FLEX_5' : None
        ,'FLEX_6' : None
        ,'FLEX_7' : None
        ,'FLEX_8' : None
        ,'FLEX_9' : None
        ,'FLEX_10' : None
        ,'FLEX_11' : None
        ,'FLEX_12' : None
        ,'FLEX_13' : None
        ,'FLEX_14' : None
        ,'FLEX_15' : None
    ,'FLEX_16' : None})