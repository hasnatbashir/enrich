import pandas as pd
import numpy as np
import locale
import json
import re
from math import ceil
import datetime
import pymssql
import sqlalchemy
import os
import sys
from config import config
import time
import ast
from recommendation_helpers import recommendation_dict, get_db_df

class Transform:
    """
    Class contains functions for transformation of data in .csv file.

    """

    def __init__(self, path):
        self.path = path
        filetype = path.split('.')[-1]
        self.data = None
        if filetype == 'xlsx':
            self.data = pd.read_excel(self.path, index_col=None, engine="openpyxl")
        elif filetype == 'csv':
            self.data = pd.read_csv(self.path, encoding="ISO-8859-1")
        self.original_data = self.data.copy()
        self.__index = 0
        self.connector = None
        self.lookup_table = {}

    def call_function(self, name, **args):
        return Transform.__dict__[name](self, **args)

    # Run this method to execute json
    def execute_pipeline(self, input_json='transformations.json'):
        """
        Executes pipeline given in the json object.
        
        Parameters:
            input_json: Json object or json file path containing instructions.

        Returns:
            Dataframe
        """

        column_count = []
        enrich_start = time.perf_counter()
        for step in Transform.__parse_json(input_json):

            for params in Transform.__get_params(step):
                func, params = params
                col_count = 0
                runtime = 0
                try:
                    if params['cols'][0] == '*':
                        column_count.extend(self.data.columns)
                        col_count += len(self.data.columns)
                    else:
                        column_count.extend(params['cols'])
                        col_count += len(params['cols'])
                except:
                    column_count.extend(self.data.columns)
                    col_count += len(self.data.columns)
                    pass
                try:
                    start_time = time.perf_counter()
                    self.data = self.call_function(func, **params)
                    runtime = time.perf_counter() - start_time
                except Exception as e:
                    runtime = time.perf_counter() - start_time
                    self.__generate_function_log(function= str(func), col_count= col_count, runtime = runtime, status="Error", message= str(e))
                    if config['on_error'] == 'rollback':
                        self.data = self.original_data.copy()
                        enrich_runtime = time.perf_counter() - enrich_start
                        self.__generate_log(col_count= pd.Series(column_count).nunique(), runtime=enrich_runtime, status="Error", message= str(e))
                        # raise(e)
                        return self.data
                    else:
                        enrich_runtime = time.perf_counter() - enrich_start
                        self.__generate_log(col_count= pd.Series(column_count).nunique(), runtime=enrich_runtime, status="Error", message= str(e))
                        continue
                self.__generate_function_log(function= str(func), col_count= col_count, runtime=runtime)
            self.save_file()
        enrich_runtime = time.perf_counter() - enrich_start
        self.__generate_log(col_count= pd.Series(column_count).nunique(), runtime=enrich_runtime)
        self.save_file()
        return self.data

    # Helper methods
    @staticmethod
    def __parse_json(input_json):

        if not str(input_json)[0] == '{':
            with open(input_json) as f:
                input_json = json.load(f)
        for step in input_json:
            yield input_json[step]

    @staticmethod
    def __get_params(step):
        for func in step:
            yield func, step[func]

    def save_file(self):
        self.__index = self.__index + 1
        self.data.to_csv('data/transformations/current_{0}.csv'.format(self.__index), index=False)
        # self.data.to_csv(filename, index=False)
    
    
    def __generate_log(self, col_count, runtime, status="Done", message="Success"):
        row_count = len(self.data)
        csv_data = self.data.to_csv(index=False)
        size = sys.getsizeof(csv_data) / (2 ** 20)
        log_df = pd.DataFrame({"TIME": datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S.%f")[:-4], "STATUS_ENRICH":status, "MESSAGE":message, "ROWS_COUNT_ENRICH":row_count, "COLS_COUNT_ENRICH":col_count, "DATA_SIZE_ENRICH": size, "RUNTIME(SECONDS)":runtime}, index=[0])
        
        if not os.path.isfile('./logs/log.csv'):
            log_df.to_csv('./logs/log.csv', header='column_names', index=False)
        else:
            log_df.to_csv('./logs/log.csv', mode='a', header=False, index=False)

    def __generate_function_log(self, function, col_count, runtime, status="Done", message="Success"):
        row_count = len(self.data)
        log_df = pd.DataFrame({"TIME": datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S.%f")[:-4],
                            "FUNCTION": function, "STATUS": status, "MESSAGE":message, "ROWS_COUNT": row_count, "COLS_COUNT": col_count, "RUNTIME(SECONDS)":runtime},
                            index=[0])
        if not os.path.isfile('./logs/log_functions.csv'):
            log_df.to_csv('./logs/log_functions.csv', header='column_names', index=False)
        else:
            log_df.to_csv('./logs/log_functions.csv', mode='a', header=False, index=False)

    def set_db_connector(self, connector):
        self.connector = connector

    def __set_db_connection(self, connection_string="mssql+pymssql://sa:CNSE@12345@192.168.223.111/ZMDB"):
        engine = sqlalchemy.create_engine(connection_string)
        connection = engine.connect()
        metadata = sqlalchemy.MetaData()
        self.connector = [engine, connection, metadata]

    def __lookup_dict_from_file(self, file_path, source, target):
        # lookup_df = pd.read_csv(file_path)
        filetype = file_path.split('.')[-1]
        lookup_df = None
        if filetype == 'xlsx':
            lookup_df = pd.read_excel(file_path, index_col=None, engine="openpyxl")
        elif filetype == 'csv':
            lookup_df = pd.read_csv(file_path, encoding="ISO-8859-1")

        lookup_df = lookup_df[[source, target]].set_index(source)
        self.lookup_table = lookup_df.to_dict()[target]

    def __lookup_dict_from_db(self, tablename="LOOKUP_VALUETABLE", source='SOURCE_VALUE', target='TARGET_VALUE'):
        self.__set_db_connection()
        engine, connection, metadata = self.connector
        lookup = sqlalchemy.Table(tablename, metadata, autoload=True, autoload_with=engine)
        query = sqlalchemy.select([lookup])
        result = connection.execute(query).fetchall()
        result_df = pd.DataFrame(result)
        result_df.columns = result[0].keys()
        result_df = result_df[[source, target]].set_index(source)
        self.lookup_table = result_df.to_dict()[target]

    def __populate_lookup_shift(self):

        self.lookup_table = {('8:00', '09:59'): 'A',
                            ('10:00', '12:59'): 'B',
                            ('13:00', '14:59'): 'C',
                            ('15:00', '17:59'): 'D',
                            ('18:00', '20:29'): 'E',
                            ('20:30', '21:59'): 'F',
                            ('22:00', '23:59'): 'G',
                            ('00:00', '01:59'): 'H',
                            ('02:00', '04:29'): 'I',
                            ('04:30', '06:59'): 'J',
                            ('07:00', '07:59'): 'K'}

    
    def lookup_phone_state(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to state abbreviation of phone no. Changes the invalid city values to null
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        # raise Exception("Error testing")
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            # self.__lookup_dict_from_db('DIM_AREACODES', 'Area_Code', 'Region')
            for c in cols:
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if ((x[0:3] in self.lookup_table) and (self.lookup_table[x[0:3]] != x)) else None)\
                    .count()

            return rows_effected

        for c in cols:
            self.__lookup_dict_from_db('DIM_AREACODES', 'Area_Code', 'Region')
            # self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            self.data.loc[self.data[c].notnull(),c + '_STATE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str)\
                .apply(lambda x: self.lookup_table[x[0:3]] if x[0:3] in self.lookup_table else None)

        return self.data

    def lookup_currency(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert country name column to currency abbreviation of country.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        # raise Exception("Error testing")
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            self.__lookup_dict_from_file('./data/currency_exchange.csv', 'Country', 'Abbreviation')
            # self.__lookup_dict_from_db('DIM_AREACODES', 'Area_Code', 'Region')
            for c in cols:
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if ((x in self.lookup_table) and (self.lookup_table[x] != x)) else None)\
                    .count()

            return rows_effected

        for c in cols:
            self.__lookup_dict_from_db('CURRENCY_LOOKUP', 'COUNTRY', 'ABBREVIATION')
            # self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            self.data.loc[self.data[c].notnull(),c + '_CURRENCY' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str)\
                .apply(lambda x: self.lookup_table[x.upper()] if x.upper() in self.lookup_table else None)

        return self.data

    def convert_currency_exchange(self, cols, currency_col, create_col=False, save_previous_changes=False, report=False):
        """
        Convert country name column to currency abbreviation of country.
        
        Parameters:
            cols: List of columns to transform.
            currency_col: Columns with currency information.
            create_col: Create a new column for transformed values.
            save_previous_changes: Save dataframe to file before transformation.
            report: Generate report of effected rows without transformation.

        Returns:
            Dataframe
        """
        # raise Exception("Error testing")
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            self.__lookup_dict_from_file('./data/currency_exchange.csv', 'Abbreviation', 'Exchange_Rate (EURO)')
            # self.__lookup_dict_from_db('DIM_AREACODES', 'Area_Code', 'Region')
            for c in cols:
                rows_effected[c] = self.data\
                    .apply(lambda x: x if (self.lookup_table[x[currency_col]] == 0.00) else None)\
                    .count()

            return rows_effected

        for c in cols:
            self.__lookup_dict_from_db('CURRENCY_LOOKUP', 'ABBREVIATION', 'EXCHANGE_RATE_EURO')
            # self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            self.data.loc[self.data[c].notnull(), c + '_EXCHANGE' if create_col else c] = self.data[self.data[currency_col].notnull()]\
                .apply(lambda x: float(self.lookup_table[x[currency_col]]) * x[c], axis=1)

        return self.data

    def __value_recommend(self, value):
        for target, group in self.lookup_table.items():
            if isinstance(group, str):
                group = ast.literal_eval(group)
            if value in group:
                return target
        return None

    def __value_recommend_count(self, value):
        for target in self.lookup_table.keys():
            if value in target:
                return target
        return None

    def lookup_recommendation(self, cols, mode='db', create_col=False, save_previous_changes=False, report=False):
        """
        Gives recommendation/suggestion for values of given column.
        
        Parameters:
            cols: List of columns to transform
            mode: Mode to get lookup from database or file.
                Options: 'db', 'file'
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(self.__value_recommend_count)\
                    .count()
            return rows_effected

        if mode == 'file':
            # Check for new groups or add values to existing groups and populate lookup dictionary
            values = []
            for c in cols:
                values.extend(self.data[c].dropna().astype(str).str.lower().unique())
            self.lookup_table = recommendation_dict(values)
            self.__set_db_connection()
            get_db_df(list(map(str, self.lookup_table.values())), list(self.lookup_table.keys()))\
                .to_sql('LOOKUP_VALUETABLE_1', con = self.connector[0], if_exists = 'replace', index=False)
        if mode == 'db':
            self.__set_db_connection()
            self.__lookup_dict_from_db('LOOKUP_VALUETABLE_1')
            temp_dict = {}
            for group, target in self.lookup_table.items():
                temp_dict[target] = ast.literal_eval(group)
            self.lookup_table = temp_dict
        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_SUGGESTION' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lower()\
                .apply(self.__value_recommend)

        return self.data

    def lookup_recommendation_columns(self, save_previous_changes=False):
        """
        Gives recommendation/suggestion for column names.
        
        Parameters:
            save_previous_changes: Save dataframe to file before transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()


        # Check for new groups or add values to existing groups and populate lookup dictionary
        self.lookup_table = recommendation_dict(self.data.columns.astype(str), filepath= './data/recommendation_columns.csv')
        new_cols = []
        for c in self.data.columns:
            new_cols.append(self.__value_recommend(c))
        self.data.columns = new_cols

        return self.data

    def filter_by_rules(self, rule, create_col=False, save_previous_changes=False, report=False):
        """
        Filter and label column based on conditions.
        
        Parameters:
            rule: Rule string expression with conditions and assignment value.
                Syntax: 'if <condition> then <label_or_value_to_assign>'
                e.g 'if col_1 == col_2 and col_1 > 500 then some_value or label'
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()


        exp = rule.split('then')
        new_col = create_col if create_col else exp[0].strip().split()[1]
        self.data.loc[self.data.eval(exp[0].strip()[2:]), new_col] = exp[1].strip()
        #                       Condition ^                             value^

        return self.data


    ### Content Conversion Function ###

    def convert_to_valid_email(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid emails. Changes the invalid email values to None
        
        Parameters:
            cols: List of columns to transform e.g (Syntax) cols = ['col1', 'col2', 'col3'], cols = ['*']
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: True = return rows effected

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        # if ((re.match(r'^[\w\-.]+@([\w-]+\.)+[\w\-]{2,4}$', x)) 
                        # and (x.islower()))
                        if ('@' in x and '.' in x) and (not x.islower())
                    else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str)\
                    .apply(lambda x: x
                        if not('@' in x or '.' in x)
                    else None)\
                    .count(),
                    'valid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if (('@' in x and '.' in x) 
                        and (x.islower()))
                    else None)\
                    .count()
                }
                    
            return rows_effected

        # For transformation
        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_EMAIL' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lower().apply(
                lambda x: x if re.match(r'^[\w-]+@([\w-]+\.)+[\w-]{2,4}$', x) else None)

        return self.data

    def convert_to_valid_cnic(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid cnic. Changes the invalid cnic values to None
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                        .apply(lambda x: x if re.match(r'^[0-9]{12}$', x) else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str)\
                        .apply(lambda x: x if not re.match(r'^[0-9]{12}$', x) else None).count(),
                    'valid':self.data[c].astype(str)\
                        .apply(lambda x: x if re.match(r'^[0-9]{5}-[0-9]{7}-[0-9]$', x) else None).count()
                }
                    
            return rows_effected
        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_CNIC' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace('-', '').apply(
                lambda x: str(x) if re.match(r'^[0-9]{5}-[0-9]{7}-[0-9]$', str(x)) else None)

        return self.data

    def convert_to_valid_ssn(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid ssn. Changes the invalid ssn values to None
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if re.match(r'(^[0-9]{9}$)', x) 
                        else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str).str.strip()\
                    .apply(lambda x: x 
                        if not re.match(r'(^[0-9]{3}-?[0-9]{2}-?[0-9]{4}$)', x) 
                        else None)\
                    .count(),
                    'vallid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if re.match(r'(^[0-9]{3}-[0-9]{2}-[0-9]{4}$|^XXX-XX-XXXX$)', x) 
                        else None)\
                    .count()}

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_SSN' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace('-', '').apply(
                lambda x: x[0:3]+'-'+x[3:5]+'-'+x[5:9] if len(x) == 9 else None)

        return self.data

    def convert_to_valid_city(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid city. Changes the invalid city values to None
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            # self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            self.__lookup_dict_from_db()
            for c in cols:
                rows_effected[c] = {"transformed":self.data[c].astype(str)\
                    .apply(lambda x: x if ((x in self.lookup_table) and (self.lookup_table[x] != x)) else None)\
                    .count(),
                    "can_not_transform":self.data[c].astype(str)\
                    .apply(lambda x: x if (not x in self.lookup_table) else None)\
                    .count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if ((x in self.lookup_table) and (self.lookup_table[x] == x)) else None)\
                    .count()}

            return rows_effected

        for c in cols:
            # self.__populate_lookup_city()
            
            self.__lookup_dict_from_db()
            # self.__lookup_dict_from_file('./data/DIM_AREACODES.csv', 'Area_Code', 'Region')
            self.data.loc[self.data[c].notnull(), c + '_VALID_CITY' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str)\
                .apply(lambda x: self.lookup_table[x] if x in self.lookup_table else None)

        return self.data

    def convert_to_valid_zip(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid zip-code. Changes the invalid zip-code values to None
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {"transformed":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{9}$)', x) else None).count(), 
                    "can_not_transform":self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\d{9}$)', x) else None).count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{5}\s?\-\s?\d{4}$)|(^\d{5}$)', x) else None).count()
                }

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_ZIP_CODE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str) \
                .str.replace('-', '').str.lstrip().str.rstrip()\
                .apply(lambda x: x if re.match(r"(^\d{5}$)|(^\d{9}$)", x) else '') \
                .apply(lambda x: x[0:5] + ' ' + x[5:9] if len(x) == 9 else x) \
                .apply(lambda x: x if len(x) == 5 else x) \
                .apply(lambda x: None if (x == '') else x)
            #.apply(
            #    lambda x: x if re.match(r"(^\d{5}$)|(^\d{5}-\d{4}$)", x) else None)

        return self.data

    def convert_to_phone_country_code(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid phone with country code. Changes the invalid phone with country code values to None
        Assuming american format for now.
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {
                    "transformed":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if re.match(r'(^\d{11}$)', x) else None).count(), 
                    "can_not_transform":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if not re.match(r'(^\d{11}$)', x) else None).count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{11}$)|(^\d{10}$)', x) else None).count()
                }
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_PHONE_COUNTRY' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str) \
                .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                .apply(lambda x: x if re.match(r"(^\d{11}$)|(^\d{10}$)", x) else '') \
                .apply(lambda x: '+' + x[0] + ' ' + x[1:11] if len(x) == 11 else x) \
                .apply(lambda x: '+1 ' + x if len(x) == 10 else x) \
                .apply(lambda x: None if (x == '') else x)

        return self.data

    def convert_to_phone_state_code(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid phone with state code. Changes the invalid phone with state code values to None
        Assuming american format for now.
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {
                    "transformed":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if re.match(r'(^\d{11}$)|(^\d{10}$)', x) else None).count(), 
                    "can_not_transform":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if not re.match(r'(^\d{11}$)|(^\d{10}$)', x) else None).count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{11}$)|(^\d{10}$)', x) else None).count()
                }
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_PHONE_STATE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lstrip(
                "0").str.replace('+', '').str.replace('-', '') \
                .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                .apply(lambda x: x if re.match(r"(^\d{11}$)|(^\d{10}$)", x) else '') \
                .apply(lambda x: '+' + x[0] + ' ' + '(' + x[1:4] + ') ' + x[4:11] if len(x) == 11 else x) \
                .apply(lambda x: '+1 (' + x[0:3] + ') ' + x[3:10] if len(x) == 10 else x) \
                .apply(lambda x: None if (x == '') else x)

        return self.data

    def convert_to_phone_county_code(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid phone with county code. Changes the invalid phone with county code values to None
        Assuming american format for now.
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {
                    "transformed":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if re.match(r'(^\d{11}$)|(^\d{10}$)|(^\d{7}$)', x) else None).count(), 
                    "can_not_transform":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if not re.match(r'(^\d{11}$)|(^\d{10}$)|(^\d{7}$)', x) else None).count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{7}$)', x) else None).count()
                }
            
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_PHONE_COUNTY' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lstrip("0") \
                .str.replace('+', '').str.replace('-', '') \
                .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                .apply(lambda x: x if re.match(r"(^\d{11}$)|(^\d{10}$)|(^\d{7}$)", x) else '') \
                .apply(lambda x: '+' + x[0] + ' ' + '(' + x[1:4] + ') ' + x[4:7] + '-' + x[7:11]
            if len(x) == 11 else x) \
                .apply(lambda x: '+1 (' + x[0:3] + ') ' + x[3:6] + '-' + x[6:10] if len(x) == 10 else x) \
                .apply(lambda x: x[0:3] + '-' + x[3:7] if len(x) == 7 else x) \
                .apply(lambda x: None if (x == '') else x)

        return self.data

    def convert_to_phone_without_code(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert column to valid phone without country/state code.
        Assuming american format for now.
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {
                    "transformed":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if re.match(r'(^\d{11}$)|(^\d{10}$)|(^\d{7}$)', x) else None).count(), 
                    "can_not_transform":self.data[c].astype(str)\
                    .str.lstrip("0").str.replace('+', '').str.replace('-', '') \
                    .str.replace('(', '').str.replace(')', '').str.replace(' ', '') \
                    .apply(lambda x: x if not re.match(r'(^\d{11}$)|(^\d{10}$)|(^\d{7}$)|(^\d{4}$)', x) else None).count(),
                    "valid":self.data[c].astype(str)\
                    .apply(lambda x: x if re.match(r'(^\d{4}$)', x) else None).count()
                }

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_VALID_PHONE_WITHOUT_CODE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str) \
                .str.lstrip("0").str.replace(' ', '').str.replace('+', '') \
                .str.replace('(', '').str.replace(')', '') \
                .apply(lambda x: x if re.match(r"(^\d{11}$)|(^\d{10}$)|(^\d{7}$)", x) else '') \
                .apply(lambda x: x[7:11] if len(x) == 11 else x) \
                .apply(lambda x: x[6:10] if len(x) == 10 else x) \
                .apply(lambda x: x[3:7] if len(x) == 7 else x) \
                .apply(lambda x: None if (x == '') else x)
        return self.data

    def convert_to_lower_case(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to lower case
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if not x.islower() 
                        else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str).str.strip()\
                    .apply(lambda x: x 
                        if not re.match(r'(^[a-zA-Z]+$)', x) 
                        else None)\
                    .count(),
                    'vallid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if x.islower() 
                        else None)\
                    .count()}

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_LOWER_CASE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lower()

        return self.data

    def convert_to_upper_case(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to upper case
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if not x.isupper() 
                        else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str).str.strip()\
                    .apply(lambda x: x 
                        if not re.match(r'(^[a-zA-Z]+$)', x) 
                        else None)\
                    .count(),
                    'vallid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if x.isupper() 
                        else None)\
                    .count()}

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_UPPER_CASE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.upper()

        return self.data

    def convert_to_proper_case(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to proper (Capitalize first character) case
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if not x.istitle() 
                        else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str).str.strip()\
                    .apply(lambda x: x 
                        if not re.match(r'(^[a-zA-Z]+$)', x) 
                        else None)\
                    .count(),
                    'vallid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if x.istitle() 
                        else None)\
                    .count()}

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_PROPER_CASE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.capitalize()

        return self.data

    def convert_to_reference_case(self, cols, ref_string, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to reference string case
        Can handle 3 cases: 
            - REF_STRING. Converts to upper case
            - ref_string. Converts to lower case
            - Ref_String. Converts to title case (Capitalize first letter of each word)
        
        Parameters:
            cols: List of columns to transform
            ref_string: Reference string to check the case to convert to.
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if ((x.istitle() and not ref_string.istitle()) or (x.isupper() and not ref_string.isupper())
                            or (x.islower() and not ref_string.islower()))
                        else None)\
                    .count(),
                    'can_not_transform':self.data[c].astype(str).str.strip()\
                    .apply(lambda x: x 
                        if not re.match(r'(^[a-zA-Z]+$)', x) 
                        else None)\
                    .count(),
                    'vallid':self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if ((x.istitle() and ref_string.istitle()) or (x.isupper() and ref_string.isupper())
                            or (x.islower() and ref_string.islower()))
                        else None)\
                    .count()}
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_REFERENCE_CASE' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str) \
                .apply(lambda x: x.upper() if ref_string.isupper() else x) \
                .apply(lambda x: x.lower() if ref_string.islower() else x) \
                .apply(lambda x: x.title() if ref_string.istitle() else x)

        return self.data

    def convert_to_proper_string(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to proper string
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = {'transformed':self.data[c].astype(str)\
                        .apply(lambda x: x 
                            if (x[0] == ' ')
                                or (x[-1] == ' ') 
                                or ('\n' in x)
                                or (re.search(r'[^a-zA-Z0-9]+', x)) 
                            else None)\
                        .count(),
                        'can_not_transform':self.data[c].count(),
                        'valid':self.data[c].astype(str)\
                        .apply(lambda x: x 
                            if not ((x[0] == ' ')
                                or (x[-1] == ' ') 
                                or ('\n' in x)
                                or (re.search(r'[^a-zA-Z0-9]+', x))) 
                            else None)\
                        .count()}
            return rows_effected

        for c in cols:

            self.data.loc[self.data[c].notnull(), c + '_PROPER_STRING' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str)\
                .apply(lambda x: x.strip().replace(r'\\n', '').replace(r'[^a-zA-Z0-9]+', ' '))\
                .apply(self.__remove_unicode_char)

        return self.data

    def convert_to_proper_date(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Changes the values in a list of columns to proper datetime type
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = {'transformed':pd.to_datetime(self.data[c], errors='coerce').count(),
                    'can_not_transform':pd.to_datetime(self.data[c], errors='coerce').isnull().sum(),
                    'valid':None}
            return rows_effected

        for c in cols:
            self.data[c + '_PROPER_DATE' if create_col else c] = pd.to_datetime(self.data[c], errors='coerce')

        return self.data

    def merge_columns(self, cols, separator='_', create_col=False, save_previous_changes=False, report=False):
        """
        Merge all columns in list with the first columns of the list.
        
        Parameters:
            cols: List of columns to transform
            separator: Delimiter/separator to join text from miltiple columns
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = {'transformed':self.data[c].count(), 'can_not_transform':None, 'valid':None}
            return rows_effected
        for c in range(1, len(cols)):
            self.data[cols[0] + '_MERGED' if create_col else cols[0]] = self.data[cols[0] + '_MERGED' \
                if create_col and (not c == 1) else cols[0]].astype(str) + str(separator) \
                + self.data[cols[c]].astype(str)

        return self.data

    def split_columns(self, cols, delimiter, create_col=False, save_previous_changes=False, report=False):
        """
        Split a column into multiple columns based on delimiter.
        
        Parameters:
            cols: List of columns to transform
            delimiter: Delimiter based on which the column will split.
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = {'transformed':self.data[c].count(),'can_not_transform':None, 'valid':None}
            return rows_effected
        for c in cols:
            temp_split = self.data[c].astype(str).str.lstrip().str.rstrip().str.split(delimiter, expand=True).dropna(axis=1, how='all')
            temp_cols = []
            for col in temp_split.columns:
                temp_cols.append(c+'_'+str(col))
            temp_split.columns = temp_cols
            self.data = self.data.join(temp_split)

        return self.data

    def sort(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Sort dataframe according to a column
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = {'transformed':self.data[c].count(),'can_not_transform':None, 'valid':None}
            return rows_effected
        self.data = self.data.sort_values(by=cols)

        return self.data

    def replace_values(self, cols, source, target, create_col=False, save_previous_changes=False, report=False):
        """
        Replace source values in a column with target values.
        
        Parameters:
            cols: List of columns to transform
            source: Value to replace.
            target: Value to replace with
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = {'transformed':self.data[c].apply(lambda x: x if x == source else None).count(),
                'can_not_transform':None, 'valid':None}
            return rows_effected
        for c in cols:
            self.data[c + '_REPLACED_VALUES' if create_col else c] = self.data[c].replace(source, target)

        return self.data

    def remove_duplicate(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Drop duplicate values from a column. Replace the duplicate values in column with null
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].duplicated().sum()
            return rows_effected
        for c in cols:
            self.data[c + '_NO_DUPLICATES' if create_col else c] = self.data[c].drop_duplicates()

        return self.data

    def fill_null_values(self, replacement_dict= {'*':0}, create_col=False, save_previous_changes=False, report=False):
        """
        Replace null values with given value.
        
        Parameters:
            cols: List of columns to transform
            replacement_dict: Dictionary that defines the value to replace null in each column
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        # cols = self.data.columns if cols[0] == '*' else cols
        if list(replacement_dict.keys())[0] == '*':
            temp_dict = {}
            val = list(replacement_dict.values())[0]
            for i in self.data.columns:
                temp_dict[i] = val
            replacement_dict = temp_dict
        cols = list(replacement_dict.keys())
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = rows_effected[c] = {'transformed':self.data[c].isnull().sum(),
                'can_not_transform':None, 'valid':None}
            return rows_effected
        for c in cols:
            if replacement_dict[c] == 'MEAN': # Mean
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].mean())
            elif replacement_dict[c] == 'MOD': # Mode
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].mode())
            elif replacement_dict[c] == 'MEDIAN': # Median
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].median())
            elif replacement_dict[c] == 'MIN': # Smallest/Minimum Value
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].min())
            elif replacement_dict[c] == 'MAX': # Largest/Maximum Value
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].max())
            elif replacement_dict[c] == 'MAS': # Moving Average from start
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].rolling(window= 6, min_periods=1).mean())
            elif replacement_dict[c] == 'MAE': # Moving Average from end
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c][::-1].rolling(window= 6, min_periods=1).mean())
            elif replacement_dict[c] == 'MFV': # Most frequent value
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(self.data[c].mode())
            elif replacement_dict[c] == 'MRV': # Most recent value
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(method='ffill')
            else: # Constant value
                self.data[c + '_FILLED_NULL' if create_col else c] = self.data[c].fillna(replacement_dict[c])
        return self.data

    def drop_columns(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Drop list of columns from dataset.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = rows_effected[c] = {'transformed':self.data[c].count(),
                'can_not_transform':None, 'valid':None}
            return rows_effected
        cols = self.data.columns if cols[0] == '*' else cols
        self.data = self.data.drop(cols, axis = 1)

        return self.data

    def convert_to_proper_number(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Extract numbers from a string and change column type to integer
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if re.match(r'^(?=[^\s]*?[0-9])(?=[^\s]*?[a-zA-Z])[a-zA-Z0-9]*$', str(x)) 
                        else None)\
                    .count()

            return rows_effected

        for c in cols:
            new_col = c + '_PROPER_NUMBER' if create_col else c
            self.data[new_col] = self.data[c].astype(str).str.extract(r'(\d+)')
            self.data[new_col] = pd.to_numeric(self.data[new_col], errors='coerce')
            self.data[new_col] = self.data[new_col].dropna()

        return self.data

    

    ### Numerical Conversion Function ###

    def convert_to_unsigned(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert numbers to usigned numbers
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(float).astype(int)\
                    .apply(lambda x: x if x < 0 else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_DISPLAY_UNSIGNED' if create_col else c] = self.data[c].astype(int).abs()

        return self.data

    def convert_to_sign(self, cols, sign='DEFAULT', create_col=False, save_previous_changes=False, report=False):
        """
        Convert numbers in string to default sign display
        sign types: DEFAULT, ACCOUNTANT, LEADING, TRAILING, LEADING_TEXT, TRAILING_TEXT
        Parameters:
            cols: List of columns to transform
            sign: sign type to convert values to
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                if sign.upper() == 'DEFAULT':
                    rows_effected[c] = self.data[c].astype(str)\
                        .apply(lambda x: x if not re.match(r'(^\-\d+$)|(^\d+$)', str(x)) else None)\
                        .count()
                elif sign.upper() == 'ACCOUNTANT':
                    rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\(\d+\)$)|(^\d+$)', str(x)) else None)\
                    .count()
                elif sign.upper() == 'LEADING':
                    rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\+\d+$)|(^\-\d+$)', str(x)) else None)\
                    .count()
                elif sign.upper() == 'TRAILING':
                    rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\d+\+$)|(^\d+\-$)', str(x)) else None)\
                    .count()
                elif sign.upper() == 'LEADING_TEXT':
                    rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\(\+ve\)\d+$)|(^\(\-ve\)\d+$)', str(x)) else None)\
                    .count()
                elif sign.upper() == 'TRAILING_TEXT':
                    rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if not re.match(r'(^\d+\(\+ve\)$)|(^\d+\(\-ve\)$)', str(x)) else None)\
                    .count()
            return rows_effected

        for c in cols:

            if sign.upper() == 'DEFAULT':
                self.data[c + '_SIGN_DEFAULT' if create_col else c] = self.data[c].astype(int).astype(str)
            elif sign.upper() == 'ACCOUNTANT':
                self.data[c + '_SIGN_ACCOUNTANT' if create_col else c] = self.data[c].apply(
                    lambda f: '({:,.0f})'.format(f * -1) if f < 0 else f)
            elif sign.upper() == 'LEADING':
                self.data[c + '_SIGN_LEADING' if create_col else c] = self.data[c].apply(
                lambda f: '{:,.0f}'.format(f) if f <= 0 else '+{:,.0f}'.format(f))
            elif sign.upper() == 'TRAILING':
                self.data[c + '_SIGN_TRAILING' if create_col else c] = self.data[c].apply(
                lambda f: '{:,.0f}-'.format(f * -1) if f < 0 else '{:,.0f}+'.format(f))
            elif sign.upper() == 'LEADING_TEXT':
                self.data[c + '_SIGN_LEADING_TEXT' if create_col else c] = self.data[c].apply(
                lambda f: '(-ve){:,.0f}'.format(f * -1) if f < 0 else '(+ve){:,.0f}'.format(f))
            elif sign.upper() == 'TRAILING_TEXT':
                self.data[c + '_SIGN_TRAILING_TEXT' if create_col else c] = self.data[c].apply(
                lambda f: '{:,.0f}(-ve)'.format(f * -1) if f < 0 else '{:,.0f}(+ve)'.format(f))

        return self.data

    def convert_to_decimal(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert numbers to decimal
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(float).astype(str)\
                    .apply(lambda x: x if (len(x.split('.')[1]) != 2) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_DECIMAL' if create_col else c] = self.data[c].astype(float)

        return self.data

    def convert_to_round(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Rounds off number to 2 decimal places
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(float).astype(str)\
                    .apply(lambda x: x if (len(x.split('.')[1]) != 2) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_ROUND' if create_col else c] = self.data[c].astype(float).round(2)

        return self.data

    def convert_to_billions(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Show value in billions
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_numeric(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_BILLIONS' if create_col else c] = self.data[c].astype(float).dropna().apply(
                lambda n: '{:.2f}{}'.format(n / 10 ** (3 * 3), 'B'))

        return self.data

    def convert_to_millions(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Show value in millions
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_numeric(self.data[c], errors='coerce').count()

            return rows_effected
        
        for c in cols:
            # self.Convert_to_Number([c], create_col, False)
            # temp_col = c+'NUMERIC'
            self.data[c + '_MILLIONS' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 10 ** (3 * 2), 'M'))

        return self.data

    def convert_to_thousands(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Show value in thousands
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_numeric(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_THOUSANDS' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 10 ** (3 * 1), 'K'))

        return self.data

    def convert_to_trillions(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Show value in trillions
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_numeric(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            # self.Convert_to_Number([c], create_col, False)
            # temp_col = c+'NUMERIC'
            self.data[c + '_TRILLIONS' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 10 ** (3 * 4), 'T'))

        return self.data

    def convert_to_use_only_decimal(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Use only decimal part of number
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'^\d+[.,]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_DECIMAL_ONLY' if create_col else c] = self.data[c].astype(float) % 1

        return self.data

    def convert_to_use_only_whole(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Use only whole part of number
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'^\d+[.,]', x) else None).count()

            return rows_effected
        for c in cols:
            self.data[c + '_WHOLE_ONLY' if create_col else c] = self.data[c].astype(float).astype(int)

        return self.data

    def convert_to_use_only_zero(self, cols, sign='+', create_col=False, save_previous_changes=False, report=False):

        """
        Converts the value with specific sign to zero.
        
        Parameters:
            cols: List of columns to transform
            sign: Sign that need to be changed to zero. Default: +
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(float)\
                    .apply(lambda x: x 
                        if (x > 0 and (sign == '+' or sign == '(+ve)'))
                            or (x < 0 and (sign == '-' or sign == '(-ve)')) 
                        else None)\
                    .count()

            return rows_effected

        for c in cols:
            self.data[c + '_ZERO_ONLY' if create_col else c] = self.data[c].apply(lambda x: 0 if (
                    ((sign == '+' or sign == '(+ve)') and float(x) > 0) or (
                        (sign == '-' or sign == '(-ve)') and float(x) < 0)) else float(x))

        return self.data

    def convert_to_negative(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert number to negative
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                # Count the positive values in the column
                rows_effected[c] = self.data[self.data[c].astype(float) > 0].count()[c]

            return rows_effected

        for c in cols:
            # self.Convert_to_Number([c], create_col, False)
            # temp_col = c+'NUMERIC'
            self.data[c + '_NEGATIVE' if create_col else c] = self.data[c].astype(int).apply(
                lambda x: x * -1 if x > 0 else x)

        return self.data

    def convert_to_positive(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert number to positive
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                # Count the negative values in the column
                rows_effected[c] = self.data[self.data[c].astype(float) < 0].count()[c]
            return rows_effected

        for c in cols:
            self.data[c + '_POSITIVE' if create_col else c] = self.data[c].astype(int).apply(
                lambda x: x * -1 if x < 0 else x)

        return self.data

    def convert_to_american_format(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert a number to american display format string
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[0-9]', x) else None).count()

            return rows_effected
        for c in cols:
            locale.setlocale(locale.LC_ALL, 'en_us')
            self.data[c + '_AMERICAN_FORMAT' if create_col else c] = self.data[c].apply(
                lambda x: locale.format_string("%.2f", x, grouping=True))

        return self.data

    def convert_to_european_format(self, cols, create_col=False, save_previous_changes=False,report=False):
        """
        Convert a number to european display format string
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[0-9]', x) else None).count()

            return rows_effected
        for c in cols:
            locale.setlocale(locale.LC_ALL, 'en_eu')
            self.data[c + '_EUROPEAN_FORMAT' if create_col else c] = self.data[c].apply(
                lambda x: locale.format_string("%.2f", x, grouping=True))

        return self.data

    def convert_to_indian_format(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert a number to indian display format string
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[0-9]', x) else None).count()

            return rows_effected

        for c in cols:
            locale.setlocale(locale.LC_ALL, 'en_in')
            self.data[c + '_INDIAN_FORMAT' if create_col else c] = self.data[c].apply(
                lambda x: locale.format_string("%.2f", x, grouping=True))

        return self.data

    ### Date Conversion Functions ###

    @staticmethod
    def __quarter_days(v):

        day = 0
        if v.month < 4 and v.is_leap_year:
            day = (v.dayofyear % 91) + 1
        elif v.month < 4 and not v.is_leap_year:
            day = (v.dayofyear % 90) + 1
        elif 3 < v.month < 7:
            day = (v.dayofyear % 91) + 1
        else:
            day = (v.dayofyear % 92) + 1

        return day

    def convert_to_micro_seconds(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to micro-seconds
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols
        
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_MICRO_SECONDS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%y-%m-%d %H:%M:%S.%f')

        return self.data

    def convert_to_seconds(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to seconds
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation


        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_SECONDS' if create_col else c] = pd.to_datetime(self.data[c]).apply(
                lambda x: x.replace(microsecond=0)).dt.strftime('%y-%m-%d %H:%M:%S')

        return self.data

    def convert_to_minutes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to minutes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_MINUTES' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%Y-%m-%d %H:%M')

        return self.data

    def convert_to_hours(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to hours
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_HOURS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%Y-%m-%d %H')

        return self.data

    def convert_to_meridiem(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to meridiem
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').astype(str)\
                    .apply(lambda x: x if not(('AM' in x) or ('PM' in x)) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_MERIDIEM' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m-%d %I:%M:%S.%f %p')

        return self.data

    def convert_to_days(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to days
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAYS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%Y-%m-%d')

        return self.data

    def convert_to_weeks(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to weeks
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEKS' if create_col else c] = pd.to_datetime(self.data[c]).dt.isocalendar()['week']

        return self.data

    def convert_to_months(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to months
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_MONTHS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%Y-%m')

        return self.data

    def convert_to_quarters(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to quarter
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_QUARTER' if create_col else c] = pd.to_datetime(self.data[c]).dt.quarter

        return self.data

    def convert_to_years(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to years
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_YEARS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%Y')

        return self.data

    def convert_to_day_of_week(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of week
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_WEEK' if create_col else c] = pd.to_datetime(self.data[c]).dt.weekday

        return self.data

    def convert_to_day_name(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert to day name from datetime
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_NAME' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%A')

        return self.data

    def convert_to_day_of_fortnight(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of fortnight
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_FORTNIGHT' if create_col else c] = pd.to_datetime(self.data[c]).apply(
                lambda x: x.day % 15 + 1)

        return self.data

    def convert_to_day_of_month(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of month
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_MONTH' if create_col else c] = pd.to_datetime(self.data[c]).apply(lambda x: x.day)

        return self.data

    def convert_to_day_of_quarter(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of quarter
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            # self.data[c + '_DAY_QUARTER' if create_col else c] = self.__quarter_days(c)
            self.data[c + '_DAY_QUARTER' if create_col else c] = self.data[c].apply(lambda x: self.__quarter_days(x))

        return self.data

    def convert_to_day_of_half_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of half-year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_HALF_YEAR' if create_col else c] = pd.to_datetime(self.data[c]).apply(
                lambda x: x.dayofyear % 182 if (x.month < 7 and not x.is_leap_year) else x.dayofyear % 183)

        return self.data

    def convert_to_day_of_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to day no. of year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_DAY_YEAR' if create_col else c] = pd.to_datetime(self.data[c]).dt.dayofyear

        return self.data

    @staticmethod
    def __week_of_month(dt):
        """ Returns the week of the month for the specified date.
        """
        first_day = dt.replace(day=1)
        dom = dt.day
        adjusted_dom = dom + first_day.weekday()
        return int(ceil(adjusted_dom/7.0))

    @staticmethod
    def __week_of_quarter(dt):
        """ Returns the week of the quarter for the specified date.
        """
        
        one_week = datetime.timedelta(weeks=1)
        date = dt.date()
        year = dt.year
        if np.isnan(year):
            return np.datetime64('NaT')
        # Q0 = January 1, Q1 = April 1, Q2 = July 1, Q3 = October 1
        quarter = ((date.month - 1) // 3)
        quarter_start = datetime.date(year, (quarter * 3) + 1, 1)
        quarter_week_2_monday = quarter_start + datetime.timedelta(days=(7 - quarter_start.weekday()) % 7)

        if date < quarter_week_2_monday:
            week = 1
        else:
            cur_week_monday = dt.date() - datetime.timedelta(days=dt.date().weekday())
            week = int((cur_week_monday - quarter_week_2_monday) / one_week) + 2

        return week

    @staticmethod
    def __week_of_half_year(dt):
        """ Returns the week of the month for the specified date.
        """
        one_week = datetime.timedelta(weeks=1)
        date = dt.date()
        year = dt.year

        
        if np.isnan(year):
            return np.datetime64('NaT')
        hy = ((date.month - 1) // 6)
        hy_start = datetime.date(year, (hy * 6) + 1, 1)
        hy_week_2_monday = hy_start + datetime.timedelta(days=(7 - hy_start.weekday()) % 7)

        if date < hy_week_2_monday:
            week = 1
        else:
            cur_week_monday = dt.date() - datetime.timedelta(days=dt.date().weekday())
            week = int((cur_week_monday - hy_week_2_monday) / one_week) + 2

        return week

    def convert_to_week_of_fortnight(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of fortnight
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_FORTNIGHT' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: (self.__week_of_month(x) % 2)+1)

        return self.data

    def convert_to_week_of_month(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of month
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_MONTH' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: self.__week_of_month(x))

        return self.data

    def convert_to_week_of_quarter(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of quarter
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_QUARTER' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: self.__week_of_quarter(x))

        return self.data

    # INCOMPLETE
    def convert_to_week_of_half_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of half year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_HALF_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: self.__week_of_half_year(x))

        return self.data

    def convert_to_week_of_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: x.weekofyear)

        return self.data

    def convert_to_month_of_quarter(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to month no. of quarter
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_MONTH_QUARTER' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: x.month % 3 +1)

        return self.data

    def convert_to_month_of_half_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to month no. of half year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_MONTH_HALF_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: x.month % 6 +1)

        return self.data

    def convert_to_month_of_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to week no. of year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_WEEK_HALF_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: x.month)

        return self.data

    def convert_to_quarter_of_half_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to quarter no. of half year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_QUARTER_HALF_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: 1 if ((x.month <= 3) or (7 <= x.month <= 9)) else 2)

        return self.data

    def convert_to_quarter_of_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to quarter no. of year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        quarters = {1:1,2:1,3:1,4:2,5:2,6:2,7:3,8:3,9:3,10:4,11:4,12:4}
        for c in cols:
            self.data[c + '_QUARTER_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: quarters[x.month])

        return self.data

    def convert_to_half_year_of_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to half year no. of year
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_HALF_YEAR_YEAR' if create_col else c] = pd.to_datetime(self.data[c])\
                .apply(lambda x: 1 if (x.month <= 6) else 2)

        return self.data
    
    def __get_shift(self, t):
        self.__populate_lookup_shift()
        # t = t.strftime('%H:%M')
        for t_range in self.lookup_table:
            if datetime.datetime.strptime(t_range[0], '%H:%M').time() <=\
                t.time() <= datetime.datetime.strptime(t_range[1], '%H:%M').time():
                return self.lookup_table[t_range]
        return None

    def convert_to_shift_lookup(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert datetime to shift lookup table
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            self.__populate_lookup_shift()
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        self.__populate_lookup_shift()
        for c in cols:
            new_col = c + '_SHIFT_LOOKUP' if create_col else c
            self.data = self.data.dropna(subset=[c])
            self.data[new_col] = pd.to_datetime(self.data[c])\
                .apply(lambda x: self.__get_shift(x))

        return self.data

    ### Data Conversion Functions ###

    def convert_to_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 3, 'B'))

        return self.data

    def convert_to_kilo_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to kilo-bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_KILO_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 10, 'KB'))

        return self.data

    def convert_to_mega_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to mega-bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 20, 'MB'))

        return self.data

    def convert_to_giga_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to giga-bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_GIGA_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 30, 'GB'))

        return self.data

    def convert_to_tera_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to tera-bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_TERA_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 40, 'TB'))

        return self.data

    def convert_to_peta_bytes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Convert data in bits to peta-bytes
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if re.match(r'[0-9]+', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_PETA_BYTES' if create_col else c] = self.data[c].apply(
                lambda n: '{:.2f}{}'.format(n / 2 ** 50, 'PB'))

        return self.data

    ### Content Removal Function ###

    def remove_null(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove null values from column
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()


        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = self.data[c].isnull().sum()
            return rows_effected
        self.data = self.data.dropna(subset=cols)

        return self.data

    def remove_alphabets(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove alphabets from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if (re.match(r'^(?=[^\s]*?[0-9])(?=[^\s]*?[a-zA-Z])[a-zA-Z0-9]*$', x) or not x.isnumeric()) 
                        else None)\
                    .count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_ALPHABETS' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(r'[a-zA-Z]+', '')

        return self.data

    def remove_alphabets_upper(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove upper-case alphabets from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[A-Z]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_ALPHABETS_UPPER' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(r'[A-Z]', '')

        return self.data

    def remove_alphabets_lower(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove lower-case alphabets from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[a-z]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_ALPHABETS_LOWER' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(r'[a-z]', '')

        return self.data

    def remove_rows_value(self, cols, value, create_col=False, save_previous_changes=False, report=False):
        """
        Remove rows from dataframe if column equals a value.
        
        Parameters:
            cols: List of columns to transform
            value: value to check.
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if x == value else None).count()

            return rows_effected

        for c in cols:
            self.data = self.data[~self.data[c] == value]

        return self.data

    def remove_rows_substring(self, cols, substring, create_col=False, save_previous_changes=False, report=False):
        """
        Remove rows from dataframe if column contains a substring.
        
        Parameters:
            cols: List of columns to transform
            substring: substring to check.
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if x.contains(substring) else None).count()

            return rows_effected

        for c in cols:
            self.data = self.data[~self.data[c].astype(str).str.contains(substring)]

        return self.data

    def remove_null_from_numeric(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Replace null values from column with 0
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()

        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x 
                        if re.match(r'^(?=[^\s]*?[0-9])(?=[^\s]*?[a-zA-Z])[a-zA-Z0-9]*$', str(x)) 
                        else None)\
                    .count()

            return rows_effected

        for c in cols:
            new_col = c + '_NUMBER_NOT_NULL' if create_col else c
            self.data = self.data[new_col].fillna(0)

        return self.data

    def remove_numerals(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove numerals(numbers with dots and commas) from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[0-9]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_NUMERALS' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(r'[\d.,]+', '')

        return self.data

    def remove_digits(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove digits from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[0-9]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_DIGITS' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(r'\d+', '')

        return self.data

    def remove_decimal(self, cols, create_col=False, save_previous_changes=False,report=False):
        """
        Remove decimal part of number.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'^\d+[.,]', x) else None).count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_DECIMAL' if create_col else c] = self.data[c].astype(float).astype(int)

        return self.data

    def remove_invalid_date(self, cols, start_date='1970-01-01', end_date='2038-01-01', create_col=False, save_previous_changes=False, report=False):
        """
        Remove dates outside of given range.
        
        Parameters:
            cols: List of columns to transform
            start_date: Start date of range outside of which dates would be invalid. Default='1970-01-01'
            end_date: End date of range outside of which dates would be invalid. Default='2038-01-01'
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            new_col = c + '_NO_INVALID_DATE' if create_col else c
            self.data[c] = pd.to_datetime(self.data[c])
            self.data[new_col] = self.data[c][(self.data[c] > start_date) & (self.data[c] <= end_date)]
            self.data = self.data.dropna(subset=[new_col])
        return self.data

    def remove_date_time(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove date and time. converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_DATE_TIME' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%P')

        return self.data

    def remove_date(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove date part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_DATE' if create_col else c] = pd.to_datetime(self.data[c]).dt.time.astype(str)

        return self.data

    def remove_time(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove time part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_TIME' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime('%y-%m-%d')

        return self.data

    def remove_day(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove day part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_DAY' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m %H:%M:%S.%f')

        return self.data

    def remove_month(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove month part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_MONTH' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%d %H:%M:%S.%f')

        return self.data

    def remove_year(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove year part of datetime and onverts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_YEAR' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%m-%d %H:%M:%S.%f')

        return self.data

    def remove_meridiem(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove meridiem part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                # rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if (('AM' in x) or ('PM' in x)) else None).count()

            return rows_effected


        for c in cols:
            self.data[c + '_NO_MERIDIEM' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%y-%m-%d %H:%M:%S.%f')

        return self.data

    def remove_hour(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove hour part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected


        for c in cols:
            self.data[c + '_NO_HOUR' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m-%d %M:%S.%f')

        return self.data

    def remove_minutes(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove minutes part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_MINUTES' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m-%d %H:%S.%f')

        return self.data

    def remove_seconds(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove seconds part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_SECONDS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m-%d %H:%M')

        return self.data

    def remove_microseconds(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove micro-seconds part of datetime and converts it to string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                rows_effected[c] = pd.to_datetime(self.data[c], errors='coerce').count()

            return rows_effected

        for c in cols:
            self.data[c + '_NO_MICRO_SECONDS' if create_col else c] = pd.to_datetime(self.data[c]).dt.strftime(
                '%Y-%m-%d %H:%M:%S')

        return self.data

    def remove_white_spaces_all(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove all white-spaces from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if ' ' in x else None).count()

            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_WHITE_SPACES' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.replace(' ', '')

        return self.data

    def remove_white_spaces_right(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove white-spaces from right-side(end) of string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if x[-1] == ' ' else None).count()
            
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_WHITE_SPACES_RIGHT' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.rstrip()

        return self.data

    def remove_white_spaces_left(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove white-spaces from left-side(start) of string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str).apply(lambda x: x if x[0] == ' ' else None).count()
            
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_WHITE_SPACES_LEFT' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lstrip()

        return self.data

    def remove_extra_spaces(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove extra white-spaces from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if ((x[0] == ' ') or x[-1] == ' ') else None).count()
            
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_EXTRA_SPACES' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str.lstrip().str.rstrip()

        return self.data

    def remove_multiline(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove multi-line from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if '\n' in x else None).count()
            
            return rows_effected

        for c in cols:
            temp_data = self.data.copy().dropna(subset=[c])
            self.data[c + '_NO_MULTI_LINE' if create_col else c] = temp_data[c].astype(str).str.replace(r'\\n', '')

        return self.data

    def remove_special_characters(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove special characters from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols

        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.data[c].astype(str)\
                    .apply(lambda x: x if re.search(r'[^a-zA-Z0-9]+', x) else None).count()
            
            return rows_effected

        for c in cols:
            # [^a-zA-Z0-9]+
            self.data.loc[self.data[c].notnull(), c + '_NO_SPECIAL_CHARACTERS' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).str\
                .replace(r'[^a-zA-Z0-9]+', '')

        return self.data

    @staticmethod
    def __remove_unicode_char(row):

        for r in row:
            if ord(r) > 127:
                row = row.replace(r, '')
        return row

    @staticmethod
    def __count_unicode_char(row):
        count = 0
        flag = False
        for s in row:
            for r in s:
                if ord(r) > 127:
                    flag = True
                    break
            if flag:
                count += 1
                flag = False
        return count

    def remove_unicode_characters(self, cols, create_col=False, save_previous_changes=False, report=False):
        """
        Remove unicode characters from string.
        
        Parameters:
            cols: List of columns to transform
            create_col: Create a new column for transformed values
            save_previous_changes: Save dataframe to file before transformation
            report: Generate report of effected rows without transformation

        Returns:
            Dataframe
        """
        if save_previous_changes:
            self.save_file()
        cols = self.data.columns if cols[0] == '*' else cols
        # To check rows effected
        if report:
            rows_effected = {}
            for c in cols:
                
                rows_effected[c] = self.__count_unicode_char(self.data[c].astype(str))
            
            return rows_effected

        for c in cols:
            self.data.loc[self.data[c].notnull(), c + '_NO_UNICODE_CHARACTERS' if create_col else c] = self.data[self.data[c].notnull()][c].astype(str).apply(
                self.__remove_unicode_char)

        return self.data


if __name__ == "__main__":
    t = Transform('data/jtel.csv')
    # np.append(data['Status'].astype(str).str.lower().unique()[1:], ['random', 'raaandom', 'rundom'])
    print(t.lookup_recommendation(['Status']))
