import cx_Oracle
import sqlalchemy
import pandas as pd

dsn_tns = cx_Oracle.makedsn("192.168.223.111", "1521", service_name="orcl.CNSE.COM.PK")
# connection = cx_Oracle.connect(user="CNSE_TEST", password="CNSE_TEST", dsn=dsn_tns)
engine= sqlalchemy.create_engine('oracle+cx_oracle://CNSE_TEST:CNSE_TEST@%s' % dsn_tns)
pd.DataFrame({'Col_1':['abc', 'cde']}).to_sql('new_table', engine.connect(), if_exists='replace')
engine.connect()